class CreateWines < ActiveRecord::Migration[6.0]
  def change
    create_table :wines do |t|
      t.references :vinerie, foreign_key: true
      t.string :name
      t.integer :color
      t.integer :sweetness
      t.integer :alcohol_percent
      t.string :yearbook

      t.timestamps
    end
  end
end
