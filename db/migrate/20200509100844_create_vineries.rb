class CreateVineries < ActiveRecord::Migration[6.0]
  def change
    create_table :vineries do |t|
      t.string :name
      t.string :foundation_year
      t.string :contact
      t.string :location

      t.timestamps
    end
  end
end
